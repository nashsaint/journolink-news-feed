<?php

namespace App\Console\Commands;

use App\Feeds;
use App\News;
use Illuminate\Console\Command;
use SimplePie;

class newsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:fetch {feed=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news feed from the source and persist it to the local database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feed = Feeds::find($this->argument('feed'));

        /** @var SimplePie $feed */
        $simplePie = new SimplePie();
        $simplePie->set_feed_url($feed->url);
        $simplePie->set_cache_location(base_path() . '/storage/app/feed');

        $simplePie->init();

        $simplePie->handle_content_type();

        /** @var SimplePie $item */
        foreach ($simplePie->get_items() as $item) {
            // skip if feed is already exist int he db
            if (News::where('resource_id', $item->get_id())->first()) {
                continue;
            }

            if ($enclosure = $item->get_enclosure()) {
                $thumbnail = head($enclosure->get_thumbnails());
            }

            /** @var News $newsItem */
            $newsItem = new News();

            $newsItem->feeds_id = $feed->id;
            $newsItem->resource_id = $item->get_id();
            $newsItem->title = $item->get_title();
            $newsItem->permalink = $item->get_permalink();
            $newsItem->description = $item->get_description();
            $newsItem->thumbnail = isset($thumbnail) ? $thumbnail : null;
            $newsItem->content = $item->get_content();

            if ($category = $item->get_category()) {
                $newsItem->category = $category->term;
            }
            if ($author = $item->get_author()) {
                $newsItem->author = $author->name;
            }

            $newsItem->save();
        }
    }
}
