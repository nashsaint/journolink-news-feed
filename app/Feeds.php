<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
    /**
     * Allow multiple add entry
     * @var array
     */
    protected $fillable = ['url'];

    /**
     * Coverts the 'categories' column into an array
     * @var array
     */
    protected $casts = [
        'categories' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany('App\News');
    }
}
