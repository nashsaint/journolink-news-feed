<?php

namespace App\Http\Controllers;

use App\Feeds;
use App\News;
use App\Sources;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totals = [
           'sources' => Sources::all()->count(),
           'feeds'   =>Feeds::all()->count(),
           'news'    => News::all()->count(),
           'users'   => User::all()->count(),
        ];

        return view('dashboard', [
            'totals' => $totals
        ]);
    }
}
