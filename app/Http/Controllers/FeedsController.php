<?php

namespace App\Http\Controllers;

use App\Feeds;
use App\News;
use Illuminate\Http\Request;
use SimplePie;

/**
 * Class FeedsController
 * @package App\Http\Controllers
 */
class FeedsController extends Controller
{
    public function index()
    {
        $feeds = Feeds::all();

        return view('feeds.index', [
            'feeds' => $feeds,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addFeed(Request $request)
    {
        if (!$request->ajax()) {
            abort(403, 'Unauthorized action.');
        }

        if (Feeds::where('url', $request->url)->count() > 0) {
            return response()->json([
                'error' => true,
                'title' => 'Feed exists',
                'text'  => 'The feed url already exists!'
            ]);
        }

        // Add the feed
        $feed = new Feeds();
        $feed->sources_id = $request->source_id;
        $feed->url = $request->url;

        $feed->save();

        // fetch news
        $this->fetchNews($feed);

        return response()->json([
            'id'         => $feed->id,
            'url'        => $feed->url,
            'categories' => $feed->categories,
        ]);
    }

    /**
     * @param Feeds $feed
     */
    public function fetchNews(Feeds $feed)
    {
        /** @var SimplePie $feed */
        $simplePie = new SimplePie();
        $simplePie->set_feed_url($feed->url);
        $simplePie->set_cache_location(base_path() . '/storage/app/feed');

        $simplePie->init();

        $simplePie->handle_content_type();

        /** @var SimplePie $item */
        foreach ($simplePie->get_items() as $item) {
            // skip if feed is already exist int he db
            if (News::where('resource_id', $item->get_id())->first()) {
                continue;
            }

            if ($enclosure = $item->get_enclosure()) {
                $thumbnail = head($enclosure->get_thumbnails());
            }

            /** @var News $newsItem */
            $newsItem = new News();

            $newsItem->feeds_id = $feed->id;
            $newsItem->resource_id = $item->get_id();
            $newsItem->title = $item->get_title();
            $newsItem->permalink = $item->get_permalink();
            $newsItem->description = $item->get_description();
            $newsItem->thumbnail = isset($thumbnail) ? $thumbnail : null;
            $newsItem->content = $item->get_content();

            if ($category = $item->get_category()) {
                $newsItem->category = $category->term;
            }
            if ($author = $item->get_author()) {
                $newsItem->author = $author->name;
            }

            $newsItem->save();
        }
    }

    /**
     * @param Feeds $feed
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFeed(Feeds $feed)
    {
        try {
            $feed->delete();
        } catch (\Exception $e) {
            report($e);
        }

        return response()->json([
           'url' => $feed->url
        ]);
    }
}
