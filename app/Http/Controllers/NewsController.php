<?php

namespace App\Http\Controllers;

use App\News;
use App\Sources;
use Illuminate\Http\Request;

/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // We need paginator for news display
        $sources = Sources::all();

        return view('news.index', [
            'sources' => $sources,
        ]);
    }

    /**
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(News $news)
    {
        return view('news.edit', [
           'news' => $news,
        ]);
    }

    /**
     * @param Request $request
     * @param News $news
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, News $news)
    {
        $news->title = $request->title;
        $news->author = $request->author;
        $news->content = $request->content;

        $news->save();

        return redirect()->route('index-news')->with('success', sprintf("Changes to news %s has been successfully saved.", $news->id));
    }
}
