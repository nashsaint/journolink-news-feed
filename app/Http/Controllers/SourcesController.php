<?php

namespace App\Http\Controllers;

use App\Sources;
use Illuminate\Http\Request;

/**
 * Class SourcesController
 * @package App\Http\Controllers
 */
class SourcesController extends Controller
{
    /**
     * SourcesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sources = Sources::all();

        return view('sources.index', [
            'sources' => $sources
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('sources.create', [

        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $source = new Sources([
           'source_name' => $request->source_name
        ]);
        $source->save();

        return redirect()->route('sources.edit', ['slug' => $source->slug]);
    }

    /**
     * @param Request $request
     * @param Sources $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request, $slug)
    {
        $source = Sources::where('slug', $slug)->first();

        return view('sources.edit', [
            'source' => $source
        ]);
    }
}
