<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Facade as Menu;


class DefineMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('primary', function ($menu) {
            $menu->add('Dashboard', 'dashboard');
            $menu->add('News', 'news');
            $menu->add('Sources', 'sources');
        });

        return $next($request);
    }
}
