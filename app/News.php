<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class News extends Model
{
    use Sluggable;

    /**
     * Category constants
     */
    const CATEGORY_BUSINESS      = 1;
    const CATEGORY_ENTERTAINMENT = 2;
    const CATEGORY_HEALTH        = 3;
    const CATEGORY_OTHERS        = 4;
    const CATEGORY_POLITICS      = 5;
    const CATEGORY_SCIENCE       = 6;
    const CATEGORY_TECHNOLOGY    = 7;

    const CATEGORY_SHOWBIZ       = 8;
    const CATEGORY_FOOTBALL      = 9;
    const CATEGORY_UK            = 10;
    const CATEGORY_MUSIC         = 11;
    const CATEGORY_TV            = 12;
    const CATEGORY_GAMING        = 13;
    const CATEGORY_WORLD         = 14;
    const CATEGORY_SPORT         = 15;
    const CATEGORY_GOLF          = 16;
    const CATEGORY_TECH          = 17;
    const CATEGORY_TENNIS        = 18;

    /**
     * @return array
     */
    public static function categoryList()
    {
        return [
            self::CATEGORY_BUSINESS      => 'Business',
            self::CATEGORY_ENTERTAINMENT => 'Entertainment',
            self::CATEGORY_HEALTH        => 'Health',
            self::CATEGORY_POLITICS      => 'Politics',
            self::CATEGORY_SCIENCE       => 'Science',
            self::CATEGORY_TECHNOLOGY    => 'Technology',
            self::CATEGORY_OTHERS        => 'Others',

            self::CATEGORY_SHOWBIZ       => 'Showbiz',
            self::CATEGORY_FOOTBALL      => 'Football',
            self::CATEGORY_UK            => 'UK',
            self::CATEGORY_MUSIC         => 'Music',
            self::CATEGORY_TV            => 'TV',
            self::CATEGORY_GAMING        => 'Gaming',
            self::CATEGORY_WORLD         => 'World',
            self::CATEGORY_SPORT         => 'Sport',
            self::CATEGORY_GOLF          => 'Golf',
            self::CATEGORY_TECH          => 'Tech',
            self::CATEGORY_TENNIS        => 'Tennis',
        ];
    }

    /**
     * @param $category
     * @return bool
     */
    public static function categoryExists($category)
    {
        if (in_array($category, self::categoryList())) {
            return true;
        }

        return false;
    }

    protected $fillable = [
        'slug',
        'title',
        'feeds_id',
        'content',
        'permalink'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
