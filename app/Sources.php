<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Sources
 * @package App
 */
class Sources extends Model
{
    use Sluggable;

    /**
     * @var array
     */
    protected $fillable = ['source_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feeds()
    {
        return $this->hasMany('App\Feeds');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'source_name'
            ]
        ];
    }

    /**
     * Get total news for source
     * @return int
     */
    public function getTotalNews()
    {
        $totalNews = 0;
        foreach ($this->feeds as $feeds)
        {
            $totalNews += $feeds->news->count();
        }

        return $totalNews;
    }
}
