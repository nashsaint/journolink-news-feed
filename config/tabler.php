<?php

return [
    'suffix' => 'Journolink',
    'logo' => '/svg/letter-j.svg',
    'urls' => [
        'logout' => 'logout',
        'profile' => 'profile',
        'settings' => 'settings',
        'search' => 'search',
        'homepage' => '/',
        'dashboard' => '/dashboard',
        'login' => 'login',
        'post-login' => 'login',
        'forgot' => 'password/reset',
        'register' => 'register',
        'post-register' => 'register',
        'post-email' => 'password/email',
        'post-reset' => 'password/reset'
    ],
    'footer' => 'Copyright 2019. Journolink News Feeder.',
    'support' => [
        'search' => false,
        'footer-menu' => false,
    ]
];
