<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feeds_id');
            $table->string('resource_id')->nullable();
            $table->string('permalink')->nullable();
            $table->string('slug');
            $table->string('title');
            $table->string('author')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('category')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->dateTime('posted_at')->nullable();
            $table->timestamps();

            $table->foreign('feeds_id')
                ->references('id')
                ->on('feeds')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
