<?php

use Illuminate\Database\Seeder;
use App\Feeds;
use App\Sources;

class FeedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->delete();

        Feeds::create([
            'sources_id' => Sources::create([
                'source_name' => 'Metro'
            ])->id,
            'url'        => 'https://metro.co.uk/feed/',
        ]);
    }
}
