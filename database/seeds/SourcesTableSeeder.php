<?php

use Illuminate\Database\Seeder;
use App\Sources;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->delete();

        Sources::create([
            'source_name' => 'Metro',
        ]);
    }
}
