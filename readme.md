## Instructions
1. Create a database 
2. Copy `.env.example` to `.env` and specific DB details
3. Run `composer install`
4. `npm install` 
5. `php artisan migrate`
6. `php artisan db:seed`
7. `php artisan news:fetch`
8. run your local server

- Check `database/seeds/UserTableSeeder.php` for login details