window.onload = function () {
   /**
    * Initialise sources
    * @private
    */
   let _initNews = function () {
      _initEditor();
   };

   /**
    *
    * @private
    */
   let _initEditor = function () {
      $('.textarea-editor').froalaEditor();
   };


   /**
    * return
    */
   return _initNews();
};