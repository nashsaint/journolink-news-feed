window.onload = function () {
   /**
    * Initialise sources
    * @private
    */
   let _initSources = function () {
      _addingFeeds();
      _deleteFeed();
   };

   /**
    * Adding new Feed
    * @private
    */
   let _addingFeeds = function () {
      let $newFeedForm = $('.new-feed-form'),
          $dimmer = $('#newFeedModal .dimmer'),
          $feedsList = $('.feeds-list'),
          $prototype = $('<span/>', {class: 'list-group-item list-group-item-action align-items-center border-bottom pl-0 pr-0'})
              .append($('<div/>', {class: 'float-left item-url'}))
              .append($('<a/>', {class: 'float-right text-danger delete-feed-btn', href: '#'})
                  .append($('<i/>', {class: 'fe fe-trash'}))
              )
      ;

      $newFeedForm.on('submit', function (e) {
         e.preventDefault();

         $dimmer.addClass('active');

         let $item = $prototype.clone();

         let $this = $(this),
             $data = $this.serialize(),
             $url = $this.attr('action');

         $.ajax({
            type: 'post',
            url: $url,
            data: $data,
            success: function (data) {
               // show returned error if there's any
               if (data.error) {
                  swal({
                     icon: 'error',
                     title: data.title,
                     text: data.text
                  });
                  $clearForm($dimmer);
                  return;
               }

               $itemUrl = $item.find('.item-url')
               $item.find('.item-url').text(data.url);

               let $deleteBtn = $item.find('.delete-feed-btn');
                  $deleteBtn.attr('data-id', data.id);
                  $deleteBtn.attr('data-url', '/feed/' + data.id + '/delete');

               $feedsList.append($item);

               $clearForm($dimmer);
            },
            error: function (response) {
               $clearForm($dimmer);
               swal({
                  icon: 'error',
                  text: 'There was an error adding a feed.  Please try again!'
               });
            }
         });
      });
   };

   /**
    * Clear modal form
    */
   let $clearForm = function ($dimmer = false) {
      if ($dimmer) {
         $dimmer.removeClass('active');
      }

      $('#newFeedModal').modal('hide');
      $('.new-feed-form')[0].reset();
   };

   /**
    * Deleting a feed
    * @private
    */
   let _deleteFeed = function () {
      let $deleteBtn = $('.delete-feed-btn');
      $(document).on('click', '.delete-feed-btn', function (e) {
         e.preventDefault();

         swal({
            title: 'Are you sure?',
            text: 'This will delete ALL news for this feed!',
            icon: 'warning',
            buttons: ['Cancel', 'Yes, delete feed!'],
            dangerMode: true
         })
             .then((value => {
               if (value == true) {
                  let $this = $(this),
                      $sourceId = $this.data('data-sourceid'),
                      $feedId = $this.data('feedid'),
                      $url = $this.data('url')
                  ;

                  $.ajax({
                     url: $url,
                     type: 'get',
                     success: function (data) {
                        let $groupItem = $this.parent('span.list-group-item');
                        if ($groupItem.length > 0) {
                           $this.parent('span.list-group-item').remove();
                        } else {
                           $this.parents('tr').remove();
                        }
                     },
                     error: function (response) {
                        console.log(response.responseText);
                     }
                  });
               }
            }));
      });
   };

   /**
    * return
    */
   return _initSources();
};