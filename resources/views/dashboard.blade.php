@extends('tabler::layouts.main')
@section('title', 'Journolink')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3">
                <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-blue mr-3">
                      <i class="fe fe-box"></i>
                    </span>
                    <div>
                        <h4 class="m-0"><a href="{{ route('sources.index') }}">{{ $totals['sources'] }} <small>Sources</small></a></h4>
                        <small class="text-muted">external sources</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3">
                <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-green mr-3">
                      <i class="fe fe-grid"></i>
                    </span>
                    <div>
                        <h4 class="m-0"><a href="{{ route('feeds') }}">{{ $totals['feeds'] }} <small>Feeds</small></a></h4>
                        <small class="text-muted">total number of feeds</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3">
                <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-pink mr-3">
                      <i class="fe fe-list"></i>
                    </span>
                    <div>
                        <h4 class="m-0"><a href="{{ route('index-news') }}">{{ $totals['news'] }} <small>News</small></a></h4>
                        <small class="text-muted">overall news items</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card p-3">
                <div class="d-flex align-items-center">
                    <span class="stamp stamp-md bg-yellow mr-3">
                      <i class="fe fe-users"></i>
                    </span>
                    <div>
                        <h4 class="m-0">{{ $totals['users'] }} <small>Users</small></h4>
                        <small class="text-muted">registered users</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop