@extends('tabler::layouts.main')

@push('scripts')
    <script src="{{ asset('js/pages/sources.js') }}"></script>
@endpush

@section('title')
    Feeds List
@stop

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Feeds</li>
        </ol>
    </nav>
@stop

@section('content')
    @if ($feeds->count() == 0)
        <div class="alert alert-primary" role="alert">
            Feeds list empty
        </div>
    @else
        <div class="row row-cards row-deck">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                            <thead>
                            <tr>
                                <th>URL</th>
                                <th class="text-center">Created at</th>
                                <th class="text-center">Total News</th>
                                <th class="text-center">Source ID</th>
                                <th class="text-right"><i class="fe fe-settings"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($feeds as $feed)
                                    <tr>
                                        <td>{{ $feed->url }}</td>
                                        <td class="text-center">{{ $feed->created_at }}</td>
                                        <td class="text-center">
                                            <div class="mx-auto chart-circle chart-circle-xs" data-value="{{ $feed->news->count() }}" data-thickness="3" data-color="blue">
                                                <div class="chart-circle-value">{{ $feed->news->count() }}</div>
                                            </div>
                                        </td>
                                        <td class="text-center">{{ $feed->sources_id }}</td>
                                        <td class="text-right">
                                            <a href="" class="delete-feed-btn text-danger delete-feed-btn"
                                               data-url="{{ route('delete-feed', ['feed' => $feed]) }}"
                                               data-sourceid="{{ $feed->sources_id }}"
                                               data-feedid="{{ $feed->id }}"
                                            ><i class="fe fe-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop