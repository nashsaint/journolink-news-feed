@extends('tabler::layouts.main')

@section('title')
    Editing News
@stop

@section('content')
    <div class="col-12">
        <div class="row">
            <div class="card">
                <div class="card-status bg-teal"></div>
                <div class="card-header">
                    <h3 class="card-title">News Edit Form</h3>
                </div>
                <form action="{{ route('update-news', ['news' => $news]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">News Title</label>
                            <input type="text" class="form-control" name="title" value="{{ $news->title }}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Author</label>
                            <input type="text" class="form-control" name="author" value="{{ $news->author }}">
                        </div>
                        <div class="form-group">
                            <label>Category</label>
                            <div class="row">
                                <div class="selectgroup selectgroup-pills">
                                    @foreach(\App\News::categoryList() as $id => $category)
                                        <label class="selectgroup-item">
                                            <input type="checkbox" name="categories[]" value="{{ $id }}" class="selectgroup-input"
                                            @if(\App\News::categoryExists($news->$category))checked @endif>
                                            <span class="selectgroup-button">{{ $category }}</span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Content</label>
                            <textarea name="content" class="form-control textarea-editor">{{ $news->content }}</textarea>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-cyan"><i class="fe fe-check"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('js/pages/news.js') }}"></script>
@endpush