@extends('tabler::layouts.main')

@push('styles')
@endpush

@section('title')
    News List by Source
@stop

@section('card-options')
    <a href="#" class="btn btn-cyan btn-sm refresh-all-news"><i class="fe fe-refresh-ccw"></i> Refresh All</a>
@stop

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-primary alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($sources->count() == 0)
        <div class="alert alert-primary" role="alert">
            You
        </div>
    @else
        @foreach($sources as $source)
            <div class="row row-cards row-deck">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $source->source_name }}</h3>
                        </div>

                        @foreach($source->feeds as $feed)
                            <div class="card-alert alert alert-success mb-0">{{ $feed->url }}</div>
                            <div class="card-body">
                                @if($feed->news->count() == 0)
                                    <small class="text-muted">Feed empty.</small>
                                @else
                                    <div class="table-responsive">
                                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th class="text-center">Created At</th>
                                                <th>Category</th>
                                                <th class="text-right"><i class="fe fe-settings"></i></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($feed->news as $news)
                                                @if($loop->index == 10)@break @endif
                                                <tr>
                                                    <td>{!!  str_limit($news->title, 50) !!}</td>
                                                    <td class="text-center">{{ $news->created_at }}</td>
                                                    <td>{{ $news->category }}</td>
                                                    <td class="text-right">
                                                        <a href="{{ route('edit-news', ['news' => $news]) }}"><i class="fe fe-edit"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        @endforeach
    @endif
@stop

@push('scripts')
    <script src="{{ asset('js/pages/news.js') }}"></script>
@endpush