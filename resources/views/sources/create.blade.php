@extends('tabler::layouts.main')

@push('scripts')
    <script src="{{ asset('js/pages/sources.js') }}"></script>
@endpush

@section('title')
    Creating new Source
@stop

@section('content')
    <div class="col-6">
        <div class="row">
            <div class="card">
                <div class="card-status bg-teal"></div>
                <div class="card-header">
                    <h3 class="card-title">Source Form</h3>
                </div>
                <form action="{{ route('sources.store') }}" method="post">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="form-group">
                            <label class="form-label">Source Display Name</label>
                            <input type="text" class="form-control" name="source_name" value="" placeholder="Metro Feeds">
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button class="btn btn-cyan" type="submit"><i class="fe fe-plus"></i> Add Source</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop