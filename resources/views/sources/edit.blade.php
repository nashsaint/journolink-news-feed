@extends('tabler::layouts.main')

@push('scripts')
    <script src="{{ asset('js/pages/sources.js') }}"></script>
@endpush

@section('title')
    Editing Source: {{ $source->source_name }}
@stop

@section('content')
    <div class="col-6">
        <div class="row">
            <div class="card">
                <div class="card-status bg-teal"></div>
                <div class="card-header">
                    <h3 class="card-title">Source Edit</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-label">Source Name</label>
                        <input type="text" class="form-control" name="source_name" value="{{ $source->source_name }}">
                    </div>
                </div>
                <div class="card-alert alert alert-success mb-0">
                    Source Feeds
                    <div class="card-options float-right">
                        <button class="btn btn-cyan btn-sm"
                                data-toggle="modal"
                                data-target="#newFeedModal"
                                data-backdrop="static"
                                data-keyboard="false"
                        >
                            <i class="fe fe-plus"></i> add feed
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="list-group list-group-transparent mb-0 feeds-list">
                        @foreach($source->feeds as $feed)
                            <span class="list-group-item list-group-item-action align-items-center border-bottom pl-0 pr-0">
                                <div class="float-left item-url">{{ $feed->url }}</div>
                                <a href="" class="float-right text-danger delete-feed-btn"
                                   data-url="{{ route('delete-feed', ['feed' => $feed]) }}"
                                   data-sourceid="{{ $feed->sources_id }}"
                                   data-feedid="{{ $feed->id }}"
                                ><i class="fe fe-trash"></i></a>
                            </span>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('modal')
    <div class="modal" id="newFeedModal" tabindex="-1" role="dialog" aria-labelledby="newFeedModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="dimmer">
                    <div class="loader"></div>
                    <div class="dimmer-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Adding A Feed</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            </button>
                        </div>
                        <form action="{{ route('add-feed') }}" class="new-feed-form">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="form-label">Feed Url</label>
                                    <input type="text" class="form-control" name="url" placeholder="http://news.feed/xml">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="source_id" value="{{ $source->id }}">
                                <button type="submit" class="btn btn-cyan new-feed-btn"><i class="fe fe-check"></i> Add Feed</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop