@extends('tabler::layouts.main')

@push('styles')
@endpush

@section('title')
    Sources List
@stop

@section('card-options')
    <a href="{{ route('sources.create') }}" class="btn btn-cyan btn-sm"><i class="fe fe-plus"></i> Add new Source</a>
@stop

@section('content')
    @if ($sources->count() == 0)
        <div class="alert alert-primary" role="alert">
            Source list empty
        </div>
    @else
        <div class="row row-cards row-deck">
            <div class="col-12">
                <div class="card">
                    <div class="table-responsive">
                        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th class="text-center">Last fetched on</th>
                                <th class="text-center">Total Feed</th>
                                <th class="text-center">Total News</th>
                                <th class="text-right"><i class="fe fe-settings"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($sources as $source)
                                    <tr>
                                        <td>{{ $source->source_name }}</td>
                                        <td class="text-center">
                                            {{ $source->last_fetched_on ?? 'Never' }}
                                        </td>
                                        <td>
                                            <div class="mx-auto chart-circle chart-circle-xs" data-value="{{ $source->feeds->count() }}" data-thickness="3" data-color="orange">
                                                <div class="chart-circle-value">{{ $source->feeds->count() }}</div>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <div class="mx-auto chart-circle chart-circle-xs" data-value="1" data-thickness="3" data-color="blue">
                                                <div class="chart-circle-value">{{ $source->getTotalNews() }}</div>
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('sources.edit', ['slug' => $source->slug]) }}"><i class="fe fe-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop