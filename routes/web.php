<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('sources', 'SourcesController');

Route::get('/feeds/', 'FeedsController@index')->name('feeds');
Route::post('/feed/add', 'FeedsController@addFeed')->name('add-feed');
Route::get('/feed/{feed}/delete', 'FeedsController@deleteFeed')->name('delete-feed');

Route::get('/news', 'NewsController@index')->name('index-news');
Route::get('/news/{news}/edit', 'NewsController@edit')->name('edit-news');
Route::post('/news/{news}/update', 'NewsController@update')->name('update-news');